@file:Suppress("unused")

package com.xerini.interview.server.api

import java.util.concurrent.CopyOnWriteArrayList
import javax.ws.rs.*
import javax.ws.rs.core.MediaType.APPLICATION_JSON

@Path("/geo-json")
class GeoJsonService {
    private val allCoordinates = CopyOnWriteArrayList<List<Double>>()

    @GET
    @Produces(APPLICATION_JSON)
    fun getGeoJson(): GeoJsonObject {
        var geoFeatures = ArrayList<GeoJsonFeature>()
        for(coordinate in allCoordinates) {
            geoFeatures.add(GeoJsonFeature(GeometryData(coordinate)))
        }
        return GeoJsonObject(geoFeatures)
    }

    @Path("/add")
    @POST
    @Consumes(APPLICATION_JSON)
    fun addPoint(coordinates: List<Double>) {
        allCoordinates.addAll(listOf(coordinates))
    }
}

data class GeoJsonObject(val features: List<GeoJsonFeature>) {
    val type: String = "FeatureCollection"
}

data class GeoJsonFeature(val geometry: GeometryData?, val properties: Map<String, Any?> = emptyMap()) {
    val type: String = "Feature"
}

data class GeometryData(val coordinates: List<Double>) {
    val type: String = "Point"
}